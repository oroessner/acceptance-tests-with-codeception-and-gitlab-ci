<?php /** @noinspection PhpUnused */

namespace Tests\Acceptance;

class TodoCest
{
    // tests
    public function tryToSeeTheDefaultTask(\AcceptanceTester $I): void
    {
        $I->amOnPage('/');
        $I->see('test this!');
    }

    public function tryToCreateATask(\AcceptanceTester $I): void
    {
        $task = 'Task from Codeception';

        $I->amOnPage('/');
        $I->fillField('Add task:', $task);
        $I->click('Submit');

        $I->see($task);
    }
}
