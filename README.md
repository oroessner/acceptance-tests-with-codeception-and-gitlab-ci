# Code Examples for: “Acceptance Tests with Codeception and Gitlab-CI” or how to solve a hen-egg problem

Automating acceptance tests tests is a challenge. Unit tests alone are often not sufficient to ensure the functionality of a web application. However, in order to test a website with a browser, it must somehow be accessible via HTTP. But how does that work if you don't want to have dozens of different versions on the QA server and who cleans up afterwards?
In this talk I try to convey how to write acceptance tests with PHP for any website, how to automate this and also how to integrate it into an existing continuous integration environment.

## What's in here?

You can find the code examples of my talk here. They are working, and you can try everything out by yourself.

* Check all Pipelines [here](https://gitlab.com/oroessner/acceptance-tests-with-codeception-and-gitlab-ci/-/pipelines)
* The [.gitlab-ci.yml](.gitlab-ci.yml) has been optimized a bit, over the examples on the slides, to reduce code duplication.
* There is a branch [test-files](https://gitlab.com/oroessner/acceptance-tests-with-codeception-and-gitlab-ci/-/tree/test-files) which illustrates the test-files example.

## Slides

* <https://slides.com/neusta/acceptance-tests-with-codeception-and-gitlab-ci>

## License

[![Creative Commons Attribution 4.0 International License](https://i.creativecommons.org/l/by/4.0/88x31.png "Creative Commons Attribution 4.0 International License")](http://creativecommons.org/licenses/by/4.0/)    
This work is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
