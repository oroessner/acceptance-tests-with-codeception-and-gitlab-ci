# no need for a composer install here, since composer is only used for codeception, not for the "app".
FROM php:8-apache

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/

RUN set -eux; \
    install-php-extensions pdo_mysql;

WORKDIR /var/www/html/

COPY public/ /var/www/html/
