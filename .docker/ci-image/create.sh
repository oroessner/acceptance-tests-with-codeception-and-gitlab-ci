#!/usr/bin/env sh

docker build --no-cache --pull -t registry.gitlab.com/oroessner/acceptance-tests-with-codeception-and-gitlab-ci/ci-runner:latest .
docker push registry.gitlab.com/oroessner/acceptance-tests-with-codeception-and-gitlab-ci/ci-runner:latest
